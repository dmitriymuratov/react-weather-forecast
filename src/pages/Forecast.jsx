import React, {Component} from 'react';
import axios from 'axios';

import '../App.css';
import WeatherIcon from '../components/WeatherIcon';
import WeatherDetails from '../components/WeatherDetails';

export default class Forecast extends Component {

    state = {
        time: 1,
        city: '',
        country_code: '',
        forecast: [
            {
                icon: '',
                time: 1,
                temperature: '',
                weatherCode: ''
            }
        ],
        fetching: true
    };

    componentDidMount() {
        this.fetchIP();
    }

    fetchForecastData = (city = 'Kharkiv', country_code = 'UA') => {
        const baseUrl = `http://api.openweathermap.org`;
        const path = `/data/2.5/forecast`;
        const appId = `942f1cd71bbf825d69551b36aba3f5a4`;
        const query = `units=metric&lang=ua&appId=${appId}`;

        axios.get(`${baseUrl}${path}?q=${city},${country_code}&${query}`)
            .then(res => {

                const date = new Date();
                const time = date.getHours();
                const day = date.getDay();

                const data = res.data;
                const fiveDaysForecast = data.list.filter((item, index) => {
                    const forecastTime = new Date(item.dt*1000).getHours();
                    return index === 0 || (index !== 0 && forecastTime === 15)
                }).map( (item, j) => ({
                    day: day + j < 7 ? day + j : day + j - 7,
                    icon: item.weather[0].icon,
                    temperature: Math.round(item.main.temp),
                    weatherCode: item.weather[0].id
                }));

                this.setState({
                    city,
                    country_code,
                    time,
                    forecast: fiveDaysForecast,
                    fetching: false
                });

            })
            .catch(error => console.error(error));

    };

    fetchIP = () => {
        axios.get('//freegeoip.net/json/')
            .then(({city, country_code}) => {
                this.fetchForecastData(city, country_code);
            })
            .catch((error) => {
                console.error(error);
                this.fetchForecastData();
            });
    };


    render() {

        const {fetching, city, country_code, time, forecast} = this.state;
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

        return (fetching ?
                <div className="app">Loading...</div>
                :
                <div className="app" data-hour={time}>

                    <div className="main">
                        <WeatherIcon
                            icon={forecast[0].icon}
                            weatherCode={forecast[0].weatherCode}
                            time={time}/>
                        <WeatherDetails
                            city={city}
                            country_code={country_code}
                            temperature={forecast[0].temperature}
                            day={days[forecast[0].day]}/>
                    </div>

                    <div className="forecast">
                        {
                            forecast.slice(1, 5).map((item, index) =>
                                <div key={index}>
                                    <WeatherIcon
                                        icon={item.icon}
                                        weatherCode={item.weatherCode}
                                        time={time}/>
                                    <WeatherDetails
                                        city={item.city}
                                        country_code={item.country_code}
                                        temperature={item.temperature}
                                        day={days[item.day]}/>
                                </div>
                            )
                        }
                    </div>

                </div>
        )
    }

}
