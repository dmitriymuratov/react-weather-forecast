import React from 'react';

function WeatherIcon({icon, weatherCode, time}) {
    const timeOfDay = (time > 7 && time < 19) ? 'day' : 'night';
    const className = `weather-icon wi wi-owm-${timeOfDay}-${weatherCode}`;

    return <i className={className}></i>;
}

export default WeatherIcon;