import React from 'react';

function WeatherDetails({ city, country_code, temperature, day }) {
    return (
        <div className="weather-details">
            {city ? (
                <div className="city">{city}, {country_code}</div>
            ) :
                (null)
            }
            <div className="temperature">{temperature} &deg; C</div>
            <div className="day">{day}</div>
        </div>
    );
}

export default WeatherDetails;