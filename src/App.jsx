import React, {Component} from 'react';
import Navigation from './layout/Navigation';
import Main from './layout/Main';
import logo from './logo.svg';


class App extends Component {
    render() {
        return <div>
                    <header>
                        <img src={logo} alt="React logo"/>
                        <h1>React 16</h1>
                        <Navigation />
                    </header>
                    <Main/>
                </div>
    }
}

export default App;
