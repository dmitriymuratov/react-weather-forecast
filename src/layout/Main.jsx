import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from '../pages/Home';
import Forecast from '../pages/Forecast';
import Contact from '../pages/Contact';

const Main = () => (
    <Switch>
        <Route path='/' exact component={Home}/>
        <Route path='/forecast' component={Forecast}/>
        <Route path='/contact' component={Contact}/>
    </Switch>
);

export default Main;
