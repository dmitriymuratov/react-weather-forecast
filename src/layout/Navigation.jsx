import React from 'react'
import { NavLink } from 'react-router-dom';

const Navigation = () => (
    <nav className="main-menu">
        <ul>
            <li><NavLink to='/'>Home</NavLink></li>
            <li><NavLink to='/forecast'>Forecast</NavLink></li>
            <li><NavLink to='/contact'>Contact</NavLink></li>
        </ul>
    </nav>
);

export default Navigation;
